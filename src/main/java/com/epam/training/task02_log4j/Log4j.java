package com.epam.training.task02_log4j;

import org.apache.logging.log4j.*;

public class Log4j {
  private static Logger logga = LogManager.getLogger();
  public static void main(String[] args) {
    logga.trace("This is a trace message.");
    logga.debug("This is a debug message.");
    logga.info("This is an info message.");
    logga.warn("This is a warn message.");
    logga.error("This is a error message.");
    logga.fatal("This is a fatal message.");
  }
}
